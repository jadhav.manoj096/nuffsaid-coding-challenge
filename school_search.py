# Part 2: Search over school data We'd like teachers to be able to easily find the school they teach at. In order to
# do this, we'd like to offer a search feature that lets them search for their school using plain text.
#
# This feature should search over school name, city name, and state name.
# The top 3 matching results should be returned (see below for examples).
#
#
# Guidelines
#
# When a query doesn't match exactly, you'll need to come up with a set of rules to rank results. In particular,
# make sure more precise matches show up at the top of the list, and if there isn't an exact match, but there is a
# close match, some results are returned. There is no perfect set of rules, but you should come up with a set that
# improve the end user search experience as much as possible. Searches should run in real-time, meaning that they
# should return results to the user in less than 200ms. It's ok to perform data loading and processing up front that
# takes longer than this if you'd like. Create a file called school_search.py, and write a method called
# search_schools. Please implement all of these features using pure python.
#
# You may use the following libraries: csv, time, itertools
# No other libraries may be used.

import csv
import time


def read_csv():

    with open('school_data.csv', "r", encoding="utf8", errors='ignore') as fp:
        csv_reader = csv.reader(fp, delimiter=',')
        line_count = 0
        data = []
        inv_index = {}

        for row in csv_reader:
            if line_count == 0:
                # skipping the first row as column name given
                pass
            else:
                # creating text in document assuming each row is document
                data.append("%s %s %s" % (row[3], row[4], row[5]))

                # building inverted index for school name
                split_school_name = row[3].split()
                for term in split_school_name:
                    if term in inv_index.keys():
                        inv_index[term].append(len(data) - 1)
                    else:
                        inv_index.update({term: [len(data) - 1]})

                # building inverted index for city name
                split_city_name = row[4].split()
                for term in split_city_name:
                    if term in inv_index.keys():
                        inv_index[term].append(len(data) - 1)
                    else:
                        inv_index.update({term: [len(data) - 1]})

                # building inverted index for state
                if row[5] in inv_index.keys():
                    inv_index[row[5]].append(len(data) - 1)
                else:
                    inv_index.update({row[5]: [len(data) - 1]})

            line_count += 1
    return data, inv_index


# Pre-processed data and index for search
data, inv_index = read_csv()


def search_schools(search_text):

    # calculating intersection of word available in  documents: assuming each row in csv is document
    first = 0
    find_documents = None
    for term in search_text.upper().split():
        if first == 0:
            find_documents = set(inv_index[term])
        else:
            temp_intersection = find_documents.intersection(inv_index[term])
            # checking here if term found in same document
            if temp_intersection:
                find_documents = find_documents.intersection(inv_index[term])
        first += 1

    result_document = []
    for document_id in find_documents:
        result_document.append(data[document_id])
    return result_document


if __name__ == "__main__":
    # Full text search considering only whitespace in word
    search_terms = [
        "elementary school highland park",
        "jefferson belleville",
        "riverside school 44",
        "granada charter school",
        "foley high alabama",
        "KUSKOKWIM",
        "OSCAR DE LA HOYA",
        "LOS ANGELES CA"
    ]

    global_start_time = time.time()
    for search_term in search_terms:
        start_time = time.time()
        documents = search_schools(search_term)
        print("\nResult for '%s' (search took : %s s)" % (search_term, round(time.time() - start_time, 4)))
        for i, each in enumerate(documents, start=1):
            print("{}.{}".format(i, each))

    print("\nAll results return in %s seconds " % round(time.time() - global_start_time, 4))

# manojjadhav@MacBook-Pro nuffsaid-coding-challenge % python3 school_search.py
#
# Result for 'elementary school highland park' (search took : 0.0025 s)
# 1.HIGHLAND PARK ELEMENTARY SCHOOL PUEBLO CO
# 2.HIGHLAND PARK ELEMENTARY SCHOOL MUSCLE SHOALS AL
#
# Result for 'jefferson belleville' (search took : 0.0 s)
# 1.JEFFERSON ELEM SCHOOL BELLEVILLE IL
#
# Result for 'riverside school 44' (search took : 0.0006 s)
# 1.RIVERSIDE SCHOOL 44 INDIANAPOLIS IN
#
# Result for 'granada charter school' (search took : 0.0003 s)
# 1.NORTH VALLEY CHARTER ACADEMY GRANADA HILLS CA
# 2.GRANADA HILLS CHARTER HIGH GRANADA HILLS CA
#
# Result for 'foley high alabama' (search took : 0.0002 s)
# 1.FOLEY HIGH SCHOOL FOLEY AL
#
# Result for 'KUSKOKWIM' (search took : 0.0 s)
# 1.TOP OF THE KUSKOKWIM SCHOOL NIKOLAI AK
#
# Result for 'OSCAR DE LA HOYA' (search took : 0.0 s)
# 1.OSCAR DE LA HOYA ANIMO CHARTER HIGH LOS ANGELES CA
#
# All results return in 0.0037 seconds
