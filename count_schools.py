# Part 1: Load data from CSV and compute stats.
# Write a method (or a method per question if you'd prefer) that loads the data out of the CSV file
# you downloaded and computes answers to the following questions:
#
# How many total schools are in this data set?
# How many schools are in each state?
# How many schools are in each Metro-centric locale?
# What city has the most schools in it? How many schools does it have in it?
# How many unique cities have at least one school in it?
#
# Guidelines
#
# Create a file called count_schools.py, and write a method called print_counts.
# Please implement all of these features using pure python.
#
# You may use the following libraries: csv, time, itertools
# No other libraries may be used.
import csv
import time
import pprint


def read_csv():
    data = []
    with open('school_data.csv', "r") as fp:
        csv_reader = csv.reader(fp, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                # skipping the first row column name
                pass
            else:
                data.append({'NCESSCH': row[0], "LEAID": row[1], "LEANM05": row[2],
                             "SCHNAM05": row[3], "LCITY05": row[4], "LSTATE05": row[5],
                             "LATCOD": row[6], "LONCOD": row[7], "MLOCALE": row[8],
                             "ULOCALE": row[9], "status05": row[10]})
            line_count += 1
    return data, line_count - 1


def calculate_schools_per_state(data):
    school_in_each_state = {}
    for item in data:
        if item.get('LSTATE05') in school_in_each_state:
            school_in_each_state[item.get('LSTATE05')] += 1
        else:
            school_in_each_state.update({item.get('LSTATE05'): 1})
    return school_in_each_state


def calculate_schools_per_metro_centric_local(data):
    school_each_metro_centric = {}
    for i in data:
        if i.get('MLOCALE') in school_each_metro_centric:
            school_each_metro_centric[i.get('MLOCALE')] += 1
        else:
            school_each_metro_centric.update({i.get('MLOCALE'): 1})
    return school_each_metro_centric


def calculate_unique_cities(data):
    school_each_city = {}
    cnt = 0
    for i in data:
        if i.get('LCITY05') in school_each_city:
            school_each_city[i.get('LCITY05')] += 1
        else:
            cnt += 1
            school_each_city.update({i.get('LCITY05'): 1})
    return cnt


def print_counts():
    start_time = time.time()

    data, total_school = read_csv()
    print("Total Schools: %s" % total_school)

    school_in_each_state = calculate_schools_per_state(data)
    print("How many School in each state")
    pprint.pprint(school_in_each_state, depth=1)

    # How many schools are in each Metro-centric locale?
    schools_each_metro_centric = calculate_schools_per_metro_centric_local(data)
    print("How many schools are in each Metro-centric locale? ")
    pprint.pprint(schools_each_metro_centric, depth=1)

    print("What city has the most schools in it? How many schools does it have in it? ",
          max(school_in_each_state.items(), key=lambda k: k[1]))

    print("How many unique cities have at least one school in it?")
    unique_cities_count = calculate_unique_cities(data)
    print(unique_cities_count)

    print("\nAll results return in %s seconds"% round(time.time() - start_time, 4))


if __name__ == "__main__":
    print_counts()

# ###### output
# Total Schools: 34779
# How many School in each state
# {'AK': 525,
#  'AL': 1606,
#  'AR': 1174,
#  'AZ': 2147,
#  'C': 1,
#  'CA': 9969,
#  'CO': 1730,
#  'CT': 1120,
#  'DC': 229,
#  'DE': 238,
#  'FL': 4379,
#  'GA': 2513,
#  'HI': 285,
#  'IA': 1565,
#  'ID': 730,
#  'IL': 4533,
#  'IN': 2031,
#  'MD': 1,
#  'NV': 1,
#  'OH': 1,
#  'OR': 1}
# How many schools are in each Metro-centric locale?
# {'1': 4862,
#  '2': 5383,
#  '3': 8180,
#  '4': 3829,
#  '5': 270,
#  '6': 2592,
#  '7': 4276,
#  '8': 4291,
#  'N': 1096}
# ('What city has the most schools in it? How many schools does it have in it? ', ('CA', 9969))
# How many unique cities have at least one school in it?
# 4852
# total execution time required: 0.136801958084 sec
